# Ethan Puerto <epuerto96@gmail.com> 2020

import numpy as np
import os
import sys

os.chdir(os.path.dirname(os.path.abspath(__file__))) #This moves the working directory to the location of the .py file


def fileToArray(filename):                  #Takes in a file, trys to open it and populate an array. If it fails posts error and ends code execution
    try:
        array = np.loadtxt(filename, dtype=str)
        return array
    except OSError:
        print("{} does not exist in {}\\".format(filename, os.getcwd() ))   
        sys.exit(1)

        
def passwordChecker(pArray):
    totalCorrect = 0 
    for password in pArray:
        pRange = password[0].split("-")
        pLetter = password[1][0]
        if password[2].count(pLetter) >= int(pRange[0]) and password[2].count(pLetter) <= int(pRange[1]): # Checks  if the letter requirment is met by counting how many times the desired letter appears
            totalCorrect += 1
    return totalCorrect

def passwordCheckerDumbElf(pArray):
    totalCorrect = 0 
    for password in pArray:
        pRange = password[0].split("-")
        pLetter = password[1][0]
        if password[2][int(pRange[0])-1] == pLetter:#Checks if criteria is met
            p1 = True
        else:
            p1 = False
        if password[2][int(pRange[1])-1] == pLetter:#Checks if criteria is met
            p2 = True
        else:
            p2 = False
        if p1 != p2: #Only true when one of them is true
            totalCorrect += 1
            print(password)
    return totalCorrect
            
        


if __name__ == "__main__":
    puzzleData = fileToArray("data.txt")    #Calls fileToArray and passes in data.txt as the filename
    print(passwordChecker(puzzleData))
    print(passwordCheckerDumbElf(puzzleData))

    
    
