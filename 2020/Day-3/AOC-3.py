# Ethan Puerto <epuerto96@gmail.com> 2020

import numpy as np
import os
import sys

os.chdir(os.path.dirname(os.path.abspath(__file__))) #This moves the working directory to the location of the .py file


def fileToArray(filename):                  #Takes in a file, trys to open it and populate an array. If it fails posts error and ends code execution
    try:
        array = np.loadtxt(filename, dtype=str, comments=" ")
        return array
    except OSError:
        print("{} does not exist in {}\\".format(filename, os.getcwd() ))   
        sys.exit(1)

def treeCounter(pArray):
    y = 1
    
    for row in pArray:
        column = 0
        print(row, row[column % len(row)])
        column += 3




if __name__ == "__main__":
    puzzleData = fileToArray("data.txt")    #Calls fileToArray and passes in data.txt as the filename
    print(treeCounter(puzzleData))
    
    
