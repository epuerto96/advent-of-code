# Ethan Puerto <epuerto96@gmail.com> 2020

import numpy as np
import os
import sys

os.chdir(os.path.dirname(os.path.abspath(__file__))) #This moves the working directory to the location of the .py file


def fileToArray(filename):                  #Takes in a file, trys to open it and populate an array. If it fails posts error and ends code execution
    try:
        array = np.loadtxt(filename, dtype=float)
        return array
    except OSError:
        print("{} does not exist in {}\\".format(filename, os.getcwd() ))   
        sys.exit(1)


        
def sumElements(array):                     #The solution to part 1 of day 1
    for i in array:                         #Take a number from the array
        for x in array:                     #Add it to another number in the array
            if (i+x == 2020):               #If the sum is 2020 return the product as the solution
                solution1 = ("Two Element Sum Problem {} + {} = 2020 so the solution is {}".format(i,x,i*x)) #hold onto the solution
            else:
                for y in array:             #take another number from the array and adds it to i and x to see if it sums to 2020
                    if (i+x+y == 2020):
                        solution2 = ("Three Element Sum Problem {} + {} + {} = 2020 so the solution is {}".format(i,x,y,i*x*y)) #hold onto the solution
    return solution1, solution2

if __name__ == "__main__":
    puzzleData = fileToArray("data.txt")    #Calls fileToArray and passes in data.txt as the filename
    day1 = sumElements(puzzleData)          #Takes the data array and sums the elements until it finds the solutions
    for i in day1:                          #Prints each solution cleanly
        print(i)

    
    
